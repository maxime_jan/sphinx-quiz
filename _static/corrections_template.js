var paragraphes = document.getElementsByTagName('p');

for (var i = 0; i < paragraphes.length; i++) {
	paragraphes[i].innerHTML = paragraphes[i].innerHTML
		.replace(/retour_ligne/g, '<br>');
}

var toggle = document.getElementsByTagName('i')[0];
toggle.style.position = 'relative';
toggle.style.top = '8px';
