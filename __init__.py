#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import string
import ast
from docutils import nodes
from docutils.parsers.rst import directives
from sphinx.util.compat import Directive

class quiz(nodes.General, nodes.Element): pass
def depart_quiz_node(self, node): pass
def depart_quiz_html(self, node): pass
def depart_quiz_latex(self, node): pass

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def visit_quiz_html(self, node):
    question = node['question']
    answers = node['answers']
    correction = node['correction']
    genre = node['genre']
    id = node['id']
    btn_correction = node['btn_correction']
    
    q = question[0]['question']
    a = answers[0]['answers']
    g = genre[0]['genre']
    c = correction[0]['correction']
    c = ast.literal_eval(c)
    
    if 'btn_correction' in btn_correction[0]:
        bc = str(btn_correction[0]['btn_correction'])
    else:
        bc = 'none'
    
    for i in range(len(c)):
        if c[i] == False:
            c[i]= "false"
        elif c[i] == True:
            c[i]= "true"
    
    divID = id_generator()
    self.body.append('<div id="' + divID + '" class="quiz '+id+'"></div>')
    self.body.append('<script type="text/javascript" src="_static/quiz.js"></script>')
    self.body.append('<script>'+g+'("'+divID+'",'+q+','+a+','+str(c)+');</script>')
    if bc != 'none':
        self.body.append('<div onclick="corriger(\''+id+'\');" class="btn btn-success">Corriger</div>')
        self.body.append('<div style="margin-bottom:-10px; margin-top:-10px;" id="resultats_'+bc+'"></div>')
    
def visit_quiz_latex(self, node):
    question = node['question']
    answers = node['answers']
    correction = node['correction']

    q = question[0]['question']
    a = answers[0]['answers']
    c = correction[0]['correction']
    
    a = ast.literal_eval(a)
    c= ast.literal_eval(c)
    q = q[1:len(q)-1]

    self.body.append("\n\\textbf{"+q+"}\n")

    for i in range (len(a)):
        self.body.append('\n\CheckBox{'+str(i+1)+'.} '+a[i]+"\n")
        
        if i == (len(a)-1):
            self.body.append("\\let\\thefootnote\\relax\\footnote{\\begin{flushright}\\begin{rotate}{180}"+q+" : ")
            
            for j in range(len(a)):
                if c[j]:
                    self.body.append(str(j+1))
                    self.body.append(" ; ")
                if j==(len(a)-1):
                    self.body.pop()
            
            self.body.append("\end{rotate}\end{flushright}}\n")
        
class Quiz(Directive):
    has_content = True
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = False
    option_spec = {
        'question': directives.unchanged,
        'answers': directives.unchanged,
        'correction': directives.unchanged,
        'genre': directives.unchanged,
        'btn_correction': directives.unchanged,
    }

    def run(self):
        answers = self.options, 'answers'
        question = self.options, 'question'
        correction = self.options, 'correction'
        btn_correction = self.options, 'btn_correction'
        genre = self.options, 'genre'
        return [quiz(id=self.arguments[0], 
                    answers=answers, 
                    question=question, 
                    correction=correction,
                    btn_correction=btn_correction,
                    genre=genre)]

def setup(app):
    app.add_stylesheet("awesome-bootstrap-checkbox.css")
    app.add_stylesheet("bootstrap-mod.css")
    app.add_stylesheet("style.css")
    app.add_javascript("quiz.js")
    app.add_javascript("corrections_template.js")
    
    app.add_latex_package("hyperref")
    app.add_latex_package("wasysym")
    app.add_latex_package("rotating")

    app.add_node(quiz, 
                html=(visit_quiz_html, depart_quiz_node), 
                latex=(visit_quiz_latex, depart_quiz_latex))
    app.add_directive('quiz', Quiz)