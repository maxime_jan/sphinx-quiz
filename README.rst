Extension de création de quiz
#############################

Cette extension Sphinx permet de créer des quiz interactifs HTML ainsi qu'une version LaTeX et PDF

* Copiez le fichier ``__init___.py`` dans ``venv/lib/python3.4/site-packages/custom/sphinx-quiz``
* Copiez le dossier ``_static`` dans ``source/``
* Ajoutez ``custom.sphinx-quiz`` dans ``conf.py`` dans la liste *extensions*
* Implémentez une directive suivant le schéma ci-dessous

Directive::

    Test quiz
    =========

    ..  quiz:: quiz1
        :question: "Quel est le meilleur système d'exploitation ?"
        :answers: ["Windows", "Mac", "Linux"]
        :correction: [False, False, True]
        :genre: radio
        
    .. quiz:: quiz1
        :question: "Lesquelles de ces expressions sont-elles égales à 0 ?"
        :answers: ["3+4-7", "2+3*0", "log(1)", "(9/9)-9", "-3+1.5*2"]
        :correction: [True, False, True, False, True]
        :genre: checkbox
        :btn_correction: quiz1

.. note::
    Prenez garde à la syntaxe, et en particulier aux guillemets. Les directives ..quiz, genre et correction ne doivent pas en contenir, tandis que question et answers en ont obligatoirement. Par ailleurs, les directives answers et correction doivent toujours être sous la forme de liste.

Aperçu
------

.. figure:: demo/html.png
.. figure:: demo/pdf.png
    :width: 70%